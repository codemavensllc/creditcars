﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Threading.Tasks;

namespace CodeSandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Attempting email");
            var result = SendEmailAsync("msanders@codemavens.com", "Testing sendgrid", "testing sendgrid from code").Result;
            Console.Write($"Result: {result.StatusCode}");
            Console.Write(result.Body.ReadAsStringAsync().Result);
            Console.WriteLine();
            Console.ReadLine();
        }

        public static Task<Response> SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var sendGridOptions = new SendGridClientOptions
            {
                ApiKey = "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"
            };
            var emailClient = new SendGridClient(sendGridOptions.ApiKey);
            var message = new SendGridMessage
            {
                //From = new EmailAddress("no-reply@flourjar.org"),
                From = new EmailAddress("no-reply@creditcars.com"),
                Subject = subject,
                HtmlContent = htmlMessage
            };
            message.AddTo(email);

            var msg = MailHelper.CreateSingleEmail(message.From, new EmailAddress(email, "Mike Sanders"), subject, htmlMessage, htmlMessage);

            //return emailClient.SendEmailAsync(message);
            var response = emailClient.SendEmailAsync(msg);

            //var temp = response.Result;

            return response;
        }
    }
}
