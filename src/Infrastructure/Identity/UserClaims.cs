﻿using CreditCars.Application.Common.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CreditCars.Infrastructure.Identity
{
    public class UserClaimsFactory : UserClaimsPrincipalFactory<ApplicationUser>
    {
        private readonly IApplicationDbContext _context;

        public UserClaimsFactory(UserManager<ApplicationUser> userManager, IOptions<IdentityOptions> optionsAccessor, IApplicationDbContext context) : base(userManager, optionsAccessor)
        {
            this._context = context;
        }

        protected override async Task<ClaimsIdentity> GenerateClaimsAsync(ApplicationUser user)
        {
            var identity = await base.GenerateClaimsAsync(user);

            //var userId = identity.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            //if (!String.IsNullOrWhiteSpace(userId))
            //{
            //    // get the congregations and circuits for this user
            //    var allCongs = identity.FindAll("Congregation");
            //    //identity.AddClaim();
            //}

            return identity;
        }
    }
}
