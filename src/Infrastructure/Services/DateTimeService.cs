﻿using CreditCars.Application.Common.Interfaces;
using System;

namespace CreditCars.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
