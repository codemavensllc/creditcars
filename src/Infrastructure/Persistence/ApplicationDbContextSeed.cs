﻿using CreditCars.Application.Common.Interfaces;
using CreditCars.Domain.Entities;
using CreditCars.Domain.Enums;
using CreditCars.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CreditCars.Infrastructure.Persistence
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedDefaultUserAsync(UserManager<ApplicationUser> userManager)
        {
            var defaultUser = new ApplicationUser { UserName = "administrator@localhost", Email = "administrator@localhost", EmailConfirmed = true  };

            if (userManager.Users.All(u => u.UserName != defaultUser.UserName))
            {
                await userManager.CreateAsync(defaultUser, "Administrator42?!");
            }
        }

        public static async Task SeedSampleDataAsync(IApplicationDbContext context)
        {
            //if (!context.Circuits.Any())
            //{
            //    var circuit = context.Circuits.Add(new Circuit()
            //    {
            //        Description = "TX-12"
            //    });

            //    var cong = context.Congregations.Add(new Congregation()
            //    {
            //        Name = "Jefferson",
            //        Circuit = circuit.Entity
            //    });
            //    var cong2 = context.Congregations.Add(new Congregation()
            //    {
            //        Name = "Marshall - East",
            //        Circuit = circuit.Entity
            //    });
            //    var cong3 = context.Congregations.Add(new Congregation()
            //    {
            //        Name = "Tyler - North",
            //        Circuit = circuit.Entity
            //    });

            //    var rice = context.ItemCategories.Add(new ItemCategory() { Category = "Rice", });
            //    var beans = context.ItemCategories.Add(new ItemCategory() { Category = "Beans" });
            //    var cleaning = context.ItemCategories.Add(new ItemCategory() { Category = "Cleaning" });
            //    var hygiene = context.ItemCategories.Add(new ItemCategory() { Category = "Hygiene" });
            //    var water = context.ItemCategories.Add(new ItemCategory() { Category = "Water" });
            //    var dryGoods = context.ItemCategories.Add(new ItemCategory() { Category = "Dry Goods" });
            //    var cannedGoods = context.ItemCategories.Add(new ItemCategory() { Category = "Canned Goods" });
            //    var veg = context.ItemCategories.Add(new ItemCategory() { Category = "Vegetables" });
            //    var drink = context.ItemCategories.Add(new ItemCategory() { Category = "Beverage" });
            //    var firstAid = context.ItemCategories.Add(new ItemCategory() { Category = "First Aid" });
            //    var meat = context.ItemCategories.Add(new ItemCategory() { Category = "Meat" });
            //    var fish = context.ItemCategories.Add(new ItemCategory() { Category = "Fish" });

            //    context.PantryLocations.AddRange(new List<PantryLocation>(){
            //        new PantryLocation() { Circuit = circuit.Entity, Description = "Circuit-Level location 1 - Warehouse" },
            //        new PantryLocation() { Circuit = circuit.Entity, Description = "Circuit-Level location 2 - Portable Trailer" },
            //        new PantryLocation() { Circuit = circuit.Entity, Congregation = cong2.Entity, Description = "Marshall East Pantry", TargetPublisherCount = 120 },
            //        new PantryLocation() { Circuit = circuit.Entity, Congregation = cong3.Entity, Description = "Tyler North Pantry", TargetPublisherCount = 150 },
            //    });

            //    var loc = context.PantryLocations.Add(new PantryLocation()
            //    {
            //        Circuit = circuit.Entity,
            //        Congregation = cong.Entity,
            //        Description = "Jefferson Pantry",
            //        TargetPublisherCount = 60
            //    });

            //    var item = context.PantryItems.Add(new PantryItem()
            //    {
            //        Quantity = 1,
            //        Measure = 20,
            //        Units = UnitOfMeasure.Pounds,
            //        Container = ContainerType.Bag,
            //        Description = "white rice",
            //        //Categories = new List<ItemCategory>() { rice.Entity, dryGoods.Entity },
            //        Location = loc.Entity
            //    });

            //    context.PantryItemCategoriesJoins.AddRange(new List<PantryItemItemCategory>()
            //    {
            //        new PantryItemItemCategory() {ItemCategory = rice.Entity,PantryItem = item.Entity },
            //        new PantryItemItemCategory() {ItemCategory = dryGoods.Entity,PantryItem = item.Entity },
            //    });

            //    var item2 = context.PantryItems.Add(new PantryItem()
            //    {
            //        Quantity = 1,
            //        Measure = 20,
            //        Units = UnitOfMeasure.Pounds,
            //        Container = ContainerType.Bag,
            //        Description = "white rice",
            //        //Categories = new List<ItemCategory>() { rice.Entity, dryGoods.Entity },
            //        Location = loc.Entity
            //    });

            //    context.PantryItemCategoriesJoins.AddRange(new List<PantryItemItemCategory>()
            //    {
            //        new PantryItemItemCategory() {ItemCategory = rice.Entity,PantryItem = item2.Entity },
            //        new PantryItemItemCategory() {ItemCategory = dryGoods.Entity,PantryItem = item2.Entity },
            //    });

            //    var item3 = context.PantryItems.Add(new PantryItem()
            //    {
            //        Quantity = 30,
            //        Measure = 2,
            //        Units = UnitOfMeasure.Pounds,
            //        Container = ContainerType.Bag,
            //        Description = "brown rice",
            //        //Categories = new List<ItemCategory>() { rice.Entity, dryGoods.Entity },
            //        Location = loc.Entity
            //    });

            //    context.PantryItemCategoriesJoins.AddRange(new List<PantryItemItemCategory>()
            //    {
            //        new PantryItemItemCategory() {ItemCategory = rice.Entity,PantryItem = item3.Entity },
            //        new PantryItemItemCategory() {ItemCategory = dryGoods.Entity,PantryItem = item3.Entity },
            //    });

            //    var item4 = context.PantryItems.Add(new PantryItem()
            //    {
            //        Quantity = 7,
            //        Measure = 14.5,
            //        Units = UnitOfMeasure.Ounces,
            //        Container = ContainerType.Can,
            //        Description = "baked beans",
            //        //Categories = new List<ItemCategory>() { beans.Entity, cannedGoods.Entity },
            //        Location = loc.Entity
            //    });

            //    context.PantryItemCategoriesJoins.AddRange(new List<PantryItemItemCategory>()
            //    {
            //        new PantryItemItemCategory() {ItemCategory = beans.Entity,PantryItem = item4.Entity },
            //        new PantryItemItemCategory() {ItemCategory = cannedGoods.Entity,PantryItem = item4.Entity },
            //    });


            //    var item5 = context.PantryItems.Add(new PantryItem()
            //    {
            //        Quantity = 13,
            //        Measure = 1,
            //        Units = UnitOfMeasure.Gallons,
            //        Container = ContainerType.Bottle,
            //        Description = "bleach",
            //        //Categories = new List<ItemCategory>() { cleaning.Entity },
            //        Location = loc.Entity
            //    });

            //    context.PantryItemCategoriesJoins.AddRange(new List<PantryItemItemCategory>()
            //    {
            //        new PantryItemItemCategory() {ItemCategory = cleaning.Entity,PantryItem = item5.Entity },
            //    });
            //    await context.SaveChangesAsync(CancellationToken.None);
            //}

            //// Seed, if necessary
            //if (!context.TodoLists.Any())
            //{
            //    context.TodoLists.Add(new TodoList
            //    {
            //        Title = "Shopping",
            //        Items =
            //        {
            //            new TodoItem { Title = "Apples", Done = true },
            //            new TodoItem { Title = "Milk", Done = true },
            //            new TodoItem { Title = "Bread", Done = true },
            //            new TodoItem { Title = "Toilet paper" },
            //            new TodoItem { Title = "Pasta" },
            //            new TodoItem { Title = "Tissues" },
            //            new TodoItem { Title = "Tuna" },
            //            new TodoItem { Title = "Water" }
            //        }
            //    });

            //    await context.SaveChangesAsync();
            //}
        }

    }
}
