﻿using CreditCars.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CreditCars.Infrastructure.Persistence.Configurations
{
    public class CustomerAnswerConfiguration : IEntityTypeConfiguration<CustomerAnswer>
    {
        public void Configure(EntityTypeBuilder<CustomerAnswer> builder)
        {
            builder.Property(_ => _.Answer)
                .HasMaxLength(256)
                .IsRequired();
        }
    }
}
