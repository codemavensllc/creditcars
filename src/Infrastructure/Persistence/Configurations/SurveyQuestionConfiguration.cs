﻿using CreditCars.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CreditCars.Infrastructure.Persistence.Configurations
{
    public class SurveyQuestionConfiguration : IEntityTypeConfiguration<SurveyQuestion>
    {
        public void Configure(EntityTypeBuilder<SurveyQuestion> builder)
        {
            builder.Property(_ => _.CategoryCode)
                .HasMaxLength(5);

            builder.Property(_ => _.QuestionText)
                .HasMaxLength(128)
                .IsRequired();
        }
    }
}
