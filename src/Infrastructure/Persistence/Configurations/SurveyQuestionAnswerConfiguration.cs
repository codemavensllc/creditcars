﻿using CreditCars.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CreditCars.Infrastructure.Persistence.Configurations
{
    public class SurveyQuestionAnswerConfiguration : IEntityTypeConfiguration<SurveyQuestionAnswer>
    {
        public void Configure(EntityTypeBuilder<SurveyQuestionAnswer> builder)
        {
            builder.Property(_ => _.AnswerText)
                .HasMaxLength(256)
                .IsRequired();

        }
    }
}
