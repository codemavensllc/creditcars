﻿using CreditCars.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CreditCars.Infrastructure.Persistence.Configurations
{
    public class CustomerScoreConfiguration : IEntityTypeConfiguration<CustomerScoreHeader>
    {
        public void Configure(EntityTypeBuilder<CustomerScoreHeader> builder)
        {
            builder.Property(_ => _.CustomerName)
                .HasMaxLength(64)
                .IsRequired();

            builder.Property(_ => _.Date)
                .IsRequired();

            builder.Property(_ => _.Address1)
                .HasMaxLength(256)
                .IsRequired(false);

            builder.Property(_ => _.Address2)
                .HasMaxLength(256)
                .IsRequired(false);

            builder.Property(_ => _.City)
                .HasMaxLength(256)
                .IsRequired(false);

            builder.Property(_ => _.State)
                .HasMaxLength(2)
                .IsRequired(false);

            builder.Property(_ => _.Zip)
                .HasMaxLength(10)
                .IsRequired(false);

            builder.Property(_ => _.Phone)
                .HasMaxLength(15)
                .IsRequired(false);

            builder.Property(_ => _.Email)
                .HasMaxLength(256)
                .IsRequired(false);

        }
    }
}
