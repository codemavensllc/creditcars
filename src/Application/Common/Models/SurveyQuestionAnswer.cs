﻿using CreditCars.Application.Common.Mappings;
using CreditCars.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Application.Common.Models
{
    public class SurveyQuestionAnswerDto : IMapFrom<SurveyQuestionAnswer>
    {
        public int Id { get; set; }

        public int QuestionId { get; set; }
        public SurveyQuestionDto Question { get; set; }

        public string AnswerText { get; set; }

        public int AnswerValue { get; set; }

        public int DisplayOrder { get; set; }
    }
}
