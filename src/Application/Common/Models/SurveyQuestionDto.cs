﻿using CreditCars.Application.Common.Mappings;
using CreditCars.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Application.Common.Models
{
    public class SurveyQuestionDto : IMapFrom<SurveyQuestion>
    {
        public int Id { get; set; }

        public string CategoryCode { get; set; }

        public string QuestionText { get; set; }

        public int DisplayOrder { get; set; }

        public int Weight { get; set; }

        public List<SurveyQuestionAnswerDto> Answers { get; set; }
    }
}
