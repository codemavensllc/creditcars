﻿using CreditCars.Application.Common.Mappings;
using CreditCars.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Application.Common.Models
{
    public class CustomerAnswerDto : IMapFrom<CustomerAnswer>
    {
        public int Id { get; set; }

        public int CustomerScoreId { get; set; }
        public CustomerScoreHeaderDto CustomerScore { get; set; }

        public int QuestionId { get; set; }
        public SurveyQuestionDto Question { get; set; }

        public string Answer { get; set; }

        public int QuestionScore { get; set; }
    }
}
