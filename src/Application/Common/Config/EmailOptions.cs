﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Application.Common.Config
{
    public class EmailOptions
    {
        public const string ConfigSection = "SendGridConfig";

        public string ApiKey { get; set; }
        public string From { get; set; }
    }
}
