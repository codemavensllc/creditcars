﻿using CreditCars.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace CreditCars.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        public DbSet<CustomerScoreHeader> CustomerScores { get; set; }

        public DbSet<CustomerAnswer> CustomerAnswers { get; set; }

        public DbSet<SurveyQuestion> SurveyQuestions { get; set; }

        public DbSet<SurveyQuestionAnswer> SurveyQuestionAnswers { get; set; }


        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        Task BeginTransactionAsync();

        Task CommitTransactionAsync();

        void RollbackTransaction();

        Task DeleteAllPantryItems();

    }
}
