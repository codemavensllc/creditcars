﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Application.Common.Interfaces
{

    public interface IEmailOptions
    {
        string ApiKey { get; set; }
    }
}
