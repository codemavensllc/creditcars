﻿using CreditCars.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Application.CustomerScoreSurvey.Queries.GetCustomerScoreSurveyQuestions
{
    public class GetCustomerScoreSurveyQuestionsVM
    {
        public List<SurveyQuestionDto> Questions { get; set; }
    }
}
