﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CreditCars.Application.Common.Interfaces;
using CreditCars.Application.Common.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CreditCars.Application.CustomerScoreSurvey.Queries.GetCustomerScoreSurveyQuestions
{
    public class GetCustomerScoreSurveyQuestionsQuery : IRequest<GetCustomerScoreSurveyQuestionsVM>
    {
        public int? Id { get; set; }
    }

    public class GetCustomerScoreSurveyQuestionsQueryHandler : IRequestHandler<GetCustomerScoreSurveyQuestionsQuery, GetCustomerScoreSurveyQuestionsVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCustomerScoreSurveyQuestionsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetCustomerScoreSurveyQuestionsVM> Handle(GetCustomerScoreSurveyQuestionsQuery request, CancellationToken cancellationToken)
        {
            var questions = await _context.SurveyQuestions
                .Where(_ => (request.Id == null || _.Id == request.Id.Value))
                .Include(_ => _.Answers)
                .OrderBy(_ => _.DisplayOrder)
                .ProjectTo<SurveyQuestionDto>(_mapper.ConfigurationProvider)
                .ToListAsync();

            var result = new GetCustomerScoreSurveyQuestionsVM() { 
                Questions = questions
            };
            return result;
        }
    }
}
