﻿using CreditCars.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Application.CustomerScoreSurvey.Commands.SaveSurveyQuestion
{
    public class SaveSurveyQuestionVM
    {
        public SurveyQuestionDto Question { get; set; }
    }
}
