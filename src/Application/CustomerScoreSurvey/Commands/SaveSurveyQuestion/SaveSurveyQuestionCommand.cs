﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CreditCars.Application.Common.Interfaces;
using CreditCars.Application.Common.Models;
using CreditCars.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CreditCars.Application.CustomerScoreSurvey.Commands.SaveSurveyQuestion
{
    public class SaveSurveyQuestionCommand : IRequest<SaveSurveyQuestionVM>
    {
        public SurveyQuestionDto Question;
    }

    public class SaveSurveyQuestionCommandHandler : IRequestHandler<SaveSurveyQuestionCommand, SaveSurveyQuestionVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public SaveSurveyQuestionCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<SaveSurveyQuestionVM> Handle(SaveSurveyQuestionCommand request, CancellationToken cancellationToken)
        {
            var questionId = request.Question.Id;

            // save or update the question
            if ( questionId > 0 )
            {
                //update
                var existingQuestion = await _context.SurveyQuestions.Where(_ => _.Id == questionId).FirstOrDefaultAsync();
                if( existingQuestion != null )
                {
                    existingQuestion.CategoryCode = request.Question.CategoryCode;
                    existingQuestion.DisplayOrder = request.Question.DisplayOrder;
                    existingQuestion.LastModified = DateTime.Now.ToUniversalTime();
                    existingQuestion.QuestionText = request.Question.QuestionText;
                    existingQuestion.Weight = request.Question.Weight;
                }
                else
                {
                    throw new ArgumentException($"Can't find existing SurveyQuestion with id {request.Question.Id}");
                }
            }
            else
            {
                //add
                var newQuestion = _context.SurveyQuestions.Add(new SurveyQuestion()
                {
                    CategoryCode = request.Question.CategoryCode,
                    Created = DateTime.Now.ToUniversalTime(),
                    DisplayOrder = request.Question.DisplayOrder,
                    QuestionText = request.Question.QuestionText,
                    Weight = request.Question.Weight
                });

                //save the id for the answers we'll add in a sec
                questionId = newQuestion.Entity.Id;
            }

            //add or update the answers
            await AddOrUpdateAnswers(questionId, request.Question.Answers);

            //commit the changes
            await _context.SaveChangesAsync(cancellationToken);

            //retrieve it with all the latest data, including answers
            var question = await _context.SurveyQuestions
                .Where(_ => _.Id == questionId)
                .Include(_ => _.Answers)
                .ProjectTo<SurveyQuestionDto>(_mapper.ConfigurationProvider)
                .FirstAsync();

            var result = new SaveSurveyQuestionVM()
            {
                Question = question
            };
            return result;
        }

        private async Task AddOrUpdateAnswers(int questionId, List<SurveyQuestionAnswerDto> answers)
        {
            foreach (var answer in answers)
            {
                // save or update the question
                if (answer.Id > 0)
                {
                    //update
                    var existingAnswer = await _context.SurveyQuestionAnswers.Where(_ => _.Id == answer.Id).FirstOrDefaultAsync();
                    if (existingAnswer != null)
                    {
                        existingAnswer.AnswerText = answer.AnswerText;
                        existingAnswer.AnswerValue = answer.AnswerValue;
                        existingAnswer.DisplayOrder = answer.DisplayOrder;
                        existingAnswer.LastModified = DateTime.Now.ToUniversalTime();
                    }
                    else
                    {
                        throw new ArgumentException($"Can't find existing SurveyQuestionAnswer with id {answer.Id}");
                    }
                }
                else
                {
                    //add
                    var newQuestion = _context.SurveyQuestionAnswers.Add(new SurveyQuestionAnswer()
                    {
                        AnswerText = answer.AnswerText,
                        AnswerValue = answer.AnswerValue,
                        Created = DateTime.Now.ToUniversalTime(),
                        DisplayOrder = answer.DisplayOrder,
                        QuestionId = questionId
                    });
                }
            }

            // intentionally not saving changes here. Letting the caller save changes to preserve transactions
        }

    }
}
