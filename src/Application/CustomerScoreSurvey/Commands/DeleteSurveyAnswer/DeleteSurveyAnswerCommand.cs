﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using CreditCars.Application.Common.Interfaces;
using CreditCars.Application.Common.Models;
using CreditCars.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CreditCars.Application.CustomerScoreSurvey.Commands.DeleteSurveyAnswer
{
    public class DeleteSurveyAnswerCommand : IRequest<DeleteSurveyAnswerVM>
    {
        public int AnswerId { get; set; }
        public int QuestionId { get; set; }
    }

    public class DeleteSurveyAnswerCommandHandler : IRequestHandler<DeleteSurveyAnswerCommand, DeleteSurveyAnswerVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public DeleteSurveyAnswerCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<DeleteSurveyAnswerVM> Handle(DeleteSurveyAnswerCommand request, CancellationToken cancellationToken)
        {
            var result = new DeleteSurveyAnswerVM(){ Success = true };

            var existingAnswer = await _context.SurveyQuestionAnswers.Where(_ => _.QuestionId == request.QuestionId && _.Id == request.AnswerId).FirstOrDefaultAsync();

            if (existingAnswer != null)
            {
                _context.SurveyQuestionAnswers.Remove(existingAnswer);

                //commit the changes
                await _context.SaveChangesAsync(cancellationToken);
            }
            else
            {
                result.Success = false;
                result.ErrorMessage = "Can't find an answer to delete!";
            }

            return result;
        }


    }
}
