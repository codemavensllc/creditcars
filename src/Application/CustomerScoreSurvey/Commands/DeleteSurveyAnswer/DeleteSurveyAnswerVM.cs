﻿using CreditCars.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Application.CustomerScoreSurvey.Commands.DeleteSurveyAnswer
{
    public class DeleteSurveyAnswerVM
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
