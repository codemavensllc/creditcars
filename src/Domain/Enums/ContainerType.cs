﻿namespace CreditCars.Domain.Enums
{
    public enum ContainerType
    {
        Bag,
        Bar,
        Box,
        Bottle,
        Can,
        Jar,
        Tub,
        Pack,
        Packet,
        Roll,
        Unknown
    }
}
