﻿namespace CreditCars.Domain.Enums
{
    public enum UnitOfMeasure
    {
        Ounces,
        Pounds,
        Gallons,
        Liters,
        Count,
        Quarts,
        Unknown
    }
}
