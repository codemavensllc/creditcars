﻿namespace CreditCars.Domain.Enums
{
    public enum PriorityLevel
    {
        None,
        Low,
        Medium,
        High
    }
}
