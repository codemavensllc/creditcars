﻿using CreditCars.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Domain.Entities
{
    public class SurveyQuestion : AuditableEntity
    {
        public int Id { get; set; }

        public string CategoryCode { get; set; }

        public string QuestionText { get; set; }

        public int DisplayOrder { get; set; }

        public int Weight { get; set; }

        public List<SurveyQuestionAnswer> Answers { get; set; }
    }
}
