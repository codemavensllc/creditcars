﻿using CreditCars.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Domain.Entities
{
    public class CustomerAnswer : AuditableEntity
    {
        public int Id { get; set; }

        public int CustomerScoreId { get; set; }
        public CustomerScoreHeader CustomerScore { get; set; }

        public int QuestionId { get; set; }
        public SurveyQuestion Question {get; set;}

        public string Answer { get; set; }

        public int QuestionScore { get; set; }
    }
}
