﻿using CreditCars.Domain.Common;

namespace CreditCars.Domain.Entities
{
    public class SurveyQuestionAnswer : AuditableEntity
    {
        public int Id { get; set; }

        public int QuestionId { get; set; }
        public SurveyQuestion Question { get; set; }

        public string AnswerText { get; set; }

        public int AnswerValue { get; set; }

        public int DisplayOrder { get; set; }
    }
}
