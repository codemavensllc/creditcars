﻿using CreditCars.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCars.Domain.Entities
{
    public class CustomerScoreHeader : AuditableEntity
    {
        public int Id { get; set; }

        public string CustomerName { get; set; }

        public DateTime Date { get; set; }

        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public int? TotalScore { get; set; }

        public List<CustomerAnswer> Answers { get; set; }
    }
}
