﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using SendGrid.Helpers.Mail;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditCars.Application.Common.Interfaces;
using CreditCars.Application.Common.Config;

namespace CreditCars.WebUI.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly EmailOptions emailOptions;

        public EmailSender(IOptions<EmailOptions> emailOptions)
        {
            this.emailOptions = emailOptions.Value;
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var sendGridOptions = new SendGridClientOptions
            {
                ApiKey = emailOptions.ApiKey
            };
            var emailClient = new SendGridClient(sendGridOptions.ApiKey);
            var message = new SendGridMessage
            {
                //From = new EmailAddress("no-reply@flourjar.org"),
                From = new EmailAddress(emailOptions.From),
                Subject = subject,
                HtmlContent = htmlMessage
            };
            message.AddTo(email);

            var msg = MailHelper.CreateSingleEmail(message.From, new EmailAddress(email), subject, htmlMessage, htmlMessage);

            return emailClient.SendEmailAsync(message);
        }

        
    }

}
