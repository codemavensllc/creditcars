﻿using CreditCars.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCars.WebUI.Models
{
    public class ViewSurveyQuestionsViewModel
    {
        public List<SurveyQuestionDto> Questions { get; set; }
    }
}
