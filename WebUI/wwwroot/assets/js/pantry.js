﻿//import { Toast } from "./lib/bootstrap/dist/js/bootstrap.bundle";
"use strict";

$(function ($) {

    var pantriesTable;
    var pantryItemsTable;

    // set up the multi-select
    $('.multi-select-picker').selectpicker();

    // set up the pantries list
    pantriesTable = $('#pantries').DataTable({
        //dom: 'Bfrtip', //l = length, f = filter, t = table itself, i = info summary, p = pagination, r = processing display element, B = buttons
        rowId: 'Id',
        buttons: [
            'copy', 'csv', 'excel', 'pdf'
        ],
        "ajax": {
            "url": "/data-entry/get-pantry-locations",
            "dataSrc": "pantryLocations"
        },
        "columns": [
            { data: 'locationLongName' },
            { data: 'targetPublisherCount' }
        ],
        //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],//options for the "Show x entries" menu
    });

    // set up the pantry item list
    pantryItemsTable = $('#pantryItems').DataTable({
        //dom: 'Bfrtip', //l = length, f = filter, t = table itself, i = info summary, p = pagination, r = processing display element, B = buttons
        dom: '<"controls"lfr><t><ip>', //put the lfr in a <div class="controls">, put the table in a separate div, put the footer in a separate div
        //dom: '<"export-options"B><"controls"lfr><t><ip>', //put the lfr in a <div class="controls">, put the table in a separate div, put the footer in a separate div
        rowId: 'Id',
        buttons: [
            'csv', 'excel', 'pdf'
        ],
        "ajax": {
            "url": "/data-entry/get-pantry-items",
            "dataSrc": "pantryItems",
            "data": function (d) {
                //have to do this to get live id
                return { "locationId": $('#LocationId').val() };
            }
        },
        "columns": [
            { data: 'location.locationLongName' },
            { data: 'combinedQuantityAndSize' },
            { data: 'description' },
            { data: 'combinedCategories' },
            { defaultContent: "<button class='btn btn-primary btn-sm edit-pantry-item'>Edit</button><button class='btn btn-dark btn-sm delete-pantry-item m-l-5'>Delete</button>" }
        ],
        //lengthMenu: [[10, 20, 50, -1], [10, 20, 50, "All"]],//options for the "Show x entries" menu
    });

    $('#pantryItems').on('click', '.edit-pantry-item', function (e) {
        var id = pantryItemsTable.row($(this).parent().parent()).data().id;

        var rowData = pantryItemsTable.row($(this).parent().parent()).data();
        var categories = $(rowData.pantryItemCategories).map(function () { return $(this)[0].itemCategoryId; }).toArray();
        //var categoryIds = $($('#Categories').find(':selected')).map(function () { return $(this).val(); }).toArray();

        $('#ItemId').val(rowData.id);
        $('#LocationId').val(rowData.location.id);
        $('#Quantity').val(rowData.quantity);
        $('#Description').val(rowData.description);
        $('#Measure').val(rowData.measure);
        $('#Units').val(rowData.units);
        $('#Container').val(rowData.container);
        $('#Categories').selectpicker('deselectAll');
        $('#Categories').selectpicker('val', categories);
        $('#AddItemTitle').text('Editing Item: ' + rowData.description);

        //categories.forEach(function(x, i){$('#Categories option[value=' + x.itemCategoryId + ']').prop('selected', true);})
        //$('#CategoryIds');


    });


    $('#pantryItems').on('click', '.cancel-edit', function (e) {
        $('#ItemId').val(0);
        $('#LocationId').val(0);
        $('#Quantity').val();
        $('#Description').val('');
        $('#Measure').val();
        $('#Units').val();
        $('#Container').val();
        $('#Categories').selectpicker('deselectAll');
        $('#AddItemTitle').text('Add Pantry Item');
    });

    $('#pantryItems').on('click', '.delete-pantry-item', function (e) {
        var rowData = pantryItemsTable.row($(this).parent().parent()).data();
        var id = rowData.id;
        var itemDescr = rowData.description;
        var itemLoc = rowData.location.locationLongName;

        swal({
            title: "Do you want to delete '" + itemDescr + "' from " + itemLoc + "?",
            text: "This action cannot be undone.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
            function () {
                $.post('/data-entry/delete-pantry-item',
                    { 'ItemId': id },
                    function (data) {
                        swal("Deleted", "Item has been deleted", "success");
                        //refresh the pantry item list
                        pantryItemsTable.ajax.reload(null, false);
                    }
                )
                    .fail(function (data) {
                        toastr.error('Failed to delete item.');
                    });
            });

    });

    //revisit this later. Use the form.addEventListener('submit', handleAjaxFormSubmit); or similar from formToJson.js
    //handle new pantry items
    $('#createPantryItem').submit(function (e) {
        e.preventDefault();
        $('#errorSummary').html(''); //clear any previous errors

        var categoryIds = $($('#Categories').find(':selected')).map(function () { return $(this).val(); }).toArray();

        var data = {
            ItemId: $(this.elements["ItemId"]).val()
            , Description: $(this.elements["Description"]).val()
            , LocationId: $(this.elements["LocationId"]).val()
            , Quantity: $(this.elements["Quantity"]).val()
            , Measure: $(this.elements["Measure"]).val()
            , Units: $(this.elements["Units"]).val()
            , Container: $(this.elements["Container"]).val()
            , CategoryIds: categoryIds
        };

        var pantryForm = this;
        var savedLocationId = data.LocationId;

        $.ajax(
            {
                type: this.method,
                url: this.action,
                //contentType: 'application/json',
                data: data,
                success: function (data) {
                    if (data.succeeded) {
                        toastr.success('Created Pantry Item');

                        //clear out the fields
                        pantryForm.reset();
                        $('.selectpicker').selectpicker('deselectAll');
                        $('#errorSummary').removeClass('alert').removeClass('alert-danger');

                        $(pantryForm.elements["LocationId"]).val(savedLocationId);

                        //refresh the pantry item list
                        pantryItemsTable.ajax.reload(null, false);
                    }
                    else {
                        if ($('#ItemId').val() > 0) {
                            toastr.error('Failed to edit Pantry Item');
                        }
                        else {
                            toastr.error('Failed to create Pantry Item');
                        }
                        var errMsg = "";

                        //show the error messages, invalidate the invalid fields
                        //data.errors
                        $.each(data.errors, function (idx, val) {
                            var parts = val.split(':');

                            var selector = '#' + parts[0];
                            if ($(selector).length > 0) {
                                if ($(selector).parents('.form-group').length > 0) {
                                    $(selector).parents('.form-group').first().addClass('has-error');
                                }
                                else {
                                    $(selector).addClass('has-error');
                                }
                            }

                            //the first part is the error field, the 2nd is the message
                            errMsg += "<div>" + parts[1] + "</div>";
                        });

                        $('#errorSummary').addClass('alert alert-danger').html(errMsg);
                    }
                },
                error: function (data) { alert(data); }
            })
            .fail(function (data) {
                alert("error");
            })
            .done(function (data) {
                //alert("complete");
            });
    });

    $('body').on("submit", 'form.fileImportForm', function (e) {
        e.preventDefault();
        e.stopPropagation();

        submitFile(e.originalEvent.target);

    });

    $('body').on('change', '#LocationId', function (e) {

        var savedLocationId = $($('#createPantryItem')[0].elements["LocationId"]).val();

        //clear the form if anything was there
        $('#createPantryItem')[0].reset();

        // put the location back in
        $($('#createPantryItem')[0].elements["LocationId"]).val(savedLocationId);

        //reload the data
        pantryItemsTable.ajax.reload(null, false);

    });
    

    async function submitFile(oFormElement) {
        var resultElement = $(oFormElement).find('[name="result"]')[0]
        const formData = new FormData(oFormElement);

        try {
            const response = await fetch(oFormElement.action, {
                method: 'POST',
                body: formData
            });

            if (response.ok) {
                //var result = response.body.json();
                toastr.success("File successfully imported");
                resultElement.value = 'File successfully imported';
            }
            else {
                toastr.error("Failed to uplaod file");
                resultElement.value = 'Result: ' + response.status + ' ' + response.statusText;
            }
        }
        catch (error) {
            resultElement.value = 'Error: ' + error;
        }

    }

    //***************************************************
    //  Charts
    //***************************************************

});

