﻿"use strict";

var exportTitle = "Congregation Pantry Items";
var congPantryItems;
var congId = 0;
var categoryName = '';

$(function () {

    $('body').on('submit', '#selectPantryForReport', function (e) {
        e.preventDefault();
        $('#errorSummary').html(''); //clear any previous errors

        var targetForm = e.originalEvent.target;
        congId = $(targetForm.elements["PantryLocationId"]).val()
        congId = congId > 0 ? congId : null;

        categoryName = $(targetForm.elements["CategoryName"]).val()

        if ((congId != null && congId > 0) || categoryName.length > 0) {
            exportTitle = $('#CongregationId :selected').text() + ' Pantry Items'
            if (categoryName != null && categoryName !== "") {
                exportTitle += ' - ' + categoryName;
            }

            window.document.title = exportTitle;
            $('#report-title').text(exportTitle);

            //show or hide congregation
            congPantryItems.columns('.congregation-column').visible(congId === null || congId === 0);

            //reload the data
            congPantryItems.ajax.reload(function (jsonData) {}, true);
        }
    });


    // set up the pantry item list
    congPantryItems = $('#congregationPantryItems').DataTable({
        rowId: 'Id',
        dom: 'lBfrtip',
        lengthMenu: [[50, 100, 200, -1], [50, 100, 200, "All"]],
        buttons: [
            /*'copy', */ 'csv',
            {
                extend: 'excel',
                text: 'Export to Excel',
                title: function () { return exportTitle; },
                messageTop: 'This is your inventory on record. Please make any updates and return to Mike Sanders (msanders@codemavens.com). If you have questions please call 407-279-2657.',
                customize: function (xlsx) {
                    var sheet = xlsx.xl.worksheets['sheet1.xml'];
                    $('row:first c', sheet).attr('s', '22'); //title row - bold w/blue background. See https://datatables.net/reference/button/excelHtml5#Customisation for additional styles
                    $('row:eq(1) c', sheet).attr('s', '55'); //caption row (55 - wrapped)
                }
            },
            {
                extend: 'pdfHtml5',
                download: 'download',
                title: function () { return exportTitle; },
                messageTop: 'This is your inventory on record. Please make any updates and return to Mike Sanders (msanders@codemavens.com). If you have questions please call 407-279-2657.',
                filename: function () { return exportTitle; },
                pageSize: 'LETTER'
            },
            'print'
        ],
        "ajax": {
            "url": "/reports/by-pantry-location",
            "type": "POST",
            "data": function (d) {
                //have to do this to get live id
                return { "pantryLocationId": congId, "categoryName": categoryName };
            },
            "dataSrc": "pantryItems"
        },
        "rowGroup": {
            dataSrc: "pantryItemCategories[0].itemCategory.category" //categoryName
        },
        "columns": [
            { data: 'location.locationLongName', "visible": false },
            { data: 'combinedCategories' },
            { data: 'description' },
            { data: 'quantity' },
            { data: 'sizeAndUnits' },//combine size & units
            { defaultContent: '&nbsp;', "visible": false}
        ],
    });

    function renderReport(container, chartData, xLabel, yTicks) {
        var plotObj = $.plot($(container), chartData, {
            xaxis: {
                tickLength: 0, // hide gridlines
                axisLabel: xLabel,
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 14,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5,
            },
            yaxis: {
                axisLabel: 'Congregation',
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 14,
                axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                axisLabelPadding: 5,
                tickLength: 0,
                ticks: yTicks,
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 0,
                borderColor: '#f0f0f0',
                labelMargin: 8,
            },
            series: {
                bar: {
                    show: true
                }
            },
            legend: {
                show: false
            },
            tooltip: {
                show: true,
                content: "%y - %x"
            }
        });
    }
});