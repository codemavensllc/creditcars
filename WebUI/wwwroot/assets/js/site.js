﻿(function ($) {
    "use strict";

    /* 
    ------------------------------------------------
    Sidebar open close animated hamburger icon
    ------------------------------------------------*/

    $(".hamburger").on('click', function () {
        $(this).toggleClass("is-active");
    });


    /*  
    -------------------
    List item active
    -------------------*/
    $('.header li, .sidebar li').on('click', function () {
        $(".header li.active, .sidebar li.active").removeClass("active");
        $(this).addClass('active');
    });

    $(".header li").on("click", function (event) {
        event.stopPropagation();
    });

    $(document).on("click", function () {
        $(".header li").removeClass("active");

    });



    /*  
    -----------------
    Chat Sidebar
    ---------------------*/


    var open = false;

    var openSidebar = function () {
        $('.chat-sidebar').addClass('is-active');
        $('.chat-sidebar-icon').addClass('is-active');
        open = true;
    }
    var closeSidebar = function () {
        $('.chat-sidebar').removeClass('is-active');
        $('.chat-sidebar-icon').removeClass('is-active');
        open = false;
    }

    $('.chat-sidebar-icon').on('click', function (event) {
        event.stopPropagation();
        var toggle = open ? closeSidebar : openSidebar;
        toggle();
    });








    /*  Auto date in footer and refresh
    --------------------------------------*/

    //document.getElementById("date-time").innerHTML = Date();

    $('.page-refresh').on("click", function () {
        location.reload();
    });


    /* TO DO LIST 
    --------------------*/
    $(".tdl-new").on('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            var v = $(this).val();
            var s = v.replace(/ +?/g, '');
            if (s == "") {
                return false;
            } else {
                $(".tdl-content ul").append("<li><label><input type='checkbox'><i></i><span>" + v + "</span><a href='#' class='ti-close'></a></label></li>");
                $(this).val("");
            }
        }
    });


    $(".tdl-content a").on("click", function () {
        var _li = $(this).parent().parent("li");
        _li.addClass("remove").stop().delay(100).slideUp("fast", function () {
            _li.remove();
        });
        return false;
    });

    // for dynamically created a tags
    $(".tdl-content").on('click', "a", function () {
        var _li = $(this).parent().parent("li");
        _li.addClass("remove").stop().delay(100).slideUp("fast", function () {
            _li.remove();
        });
        return false;
    });



    /*  Chat Sidebar User custom Search
    ---------------------------------------*/

    $('[data-search]').on('keyup', function () {
        var searchVal = $(this).val();
        var filterItems = $('[data-filter-item]');

        if (searchVal != '') {
            filterItems.addClass('hidden');
            $('[data-filter-item][data-filter-name*="' + searchVal.toLowerCase() + '"]').removeClass('hidden');
        } else {
            filterItems.removeClass('hidden');
        }
    });


    /*  Chackbox all
    ---------------------------------------*/

    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });


    /*  Vertical Carousel
    ---------------------------*/

    $('#verticalCarousel').carousel({
        interval: 2000
    })

    $(window).bind("resize", function () {
        console.log($(this).width())
        if ($(this).width() < 680) {
            $('.logo').addClass('hidden')
            $('.sidebar').removeClass('sidebar-shrink')
            $('.sidebar').removeClass('sidebar-shrink, sidebar-gestures')
        }
    }).trigger('resize');



    /*  Search
    ------------*/
    $('a[href="#search"]').on('click', function (event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });

    $('#search, #search button.close').on('click keyup', function (event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });


    //Do not include! This prevents the form from submitting for DEMO purposes only!
    //$('form').submit(function (event) {
    //    event.preventDefault();
    //    return false;
    //})



    /*  pace Loader
    -------------*/

    window.paceOptions = {
        elements: true
    };



})(jQuery);




/**
 * A utility function that shows a spinner animation in the middle of a specified element
 * @param {string} elementSelector Optional selector to reach the specified element on the DOM
 */
function showBusyWait(elementSelector) {
    if (elementSelector == null || elementSelector === "") {
        $("#busy-wait-bracket").append('<div class="busy-wait loader mx-auto">&nbsp;</div>');
    }
    else {
        $(elementSelector).append('<div class="loader mx-auto text-center">&nbsp;</div>');
    }
}

/**
 * A utility function that hides a spinner animation in the middle of a specified element
 * @param {string} elementSelector Optional selector to reach the specified element on the DOM
 */
function killBusyWait(elementSelector) {
    if (elementSelector == null || elementSelector === "") {
        $("#busy-wait-bracket .busy-wait").remove();
    }
    else {
        $(elementSelector).find('.loader').remove();
    }
}


/**
* Display an alert, and perform an action if the user clicks the Confirm button
 * @param {string} title The text to be displayed in the title bar of the alert
 * @param {string} htmlContent Well formed HTML to be used as the body text of the alert
 * @param {string} confirmButtonText What to show as the text of the confirm button, like Save or Delete, etc
 * @param {string} urlToExecute The relative path to the controller method that will be used when the confirm button is clicked
 * @param {any} data Any JSON object needed by the controller method
 * @param {any} successCallback Any function to call when the POST succeeds (example: showSuccessMessage("something meaningful"))
 * @param {any} errorCallback Any function to call when the POST fails (example: showErrorMessage("something meaningful"))
 */
function confirmationAlertWithCallbacks(title, htmlContent, confirmButtonText, urlToExecute, data, successCallback, errorCallback) {

    swal.fire({
        icon: 'warning',
        title: title,
        html: htmlContent,
        showCancelButton: true,
        confirmButtonText: confirmButtonText,
    }).then((result) => {

        if (result.isConfirmed) {
            showBusyWait();
            handlePostWithCallbacks(urlToExecute, data, successCallback, errorCallback);
        }
    })
}


function handlePostWithCallbacks(urlToPost, requestData, successCallback, failCallback) {
    $.post(urlToPost, requestData)
        .done(function (response) {
            if (successCallback !== null) {
                try {
                    successCallback(response);
                } catch (e) {
                    showGenericError();
                }
            }
            else {
                showSuccessMessage("Success");
            }
        })
        .fail(function (xhr, textStatus, errorThrown) {
            if (failCallback !== null) {
                try {
                    failCallback();
                } catch (e) {
                    showGenericError();
                }
            }
            else {
                console.log(errorThrown);
                showErrorMessage(xhr.responseText);
            }
        })
        .always(function () {
            killBusyWait();
        });
}


/**
 * Show success message toast popup, and then redirect to a page
 * @param {string} message The message to display in the toast notification
 * @param {string} urlToRedirectTo The url to go to
 */
function toastAndRedirect(message, urlToRedirectTo) {
    showSuccessMessage(message);

    setTimeout(() => { window.location.replace(urlToRedirectTo) }, 3000); // let the alert stay up for 3 seconds before redirecting
}

/**
 * Show success message toast popup, remove an element from the screen
 * @param {string} message The message to display in the toast notification
 * @param {string} elementToRemoveSelector The path selector to the element on the dom to hide
 */
function toastAndRemoveElement(message, elementToRemoveSelector) {
    showSuccessMessage(message);
    $(elementToRemoveSelector).slideUp(400, function () { $(this).remove(); });
    $('#results').removeClass('loader mx-auto');
}


function showGenericError() {
    showErrorMessage("An error ocurred. Administrators have been notified.");
}


function showErrorMessage(msg) {
    toastr.options = {
        'closeButton': true,
        'debug': false,
        'newestOnTop': false,
        'progressBar': false,
        'positionClass': 'toast-top-center',
        'preventDuplicates': false,
        'showDuration': '200',
        'hideDuration': '700',
        'timeOut': '5000',
        'extendedTimeOut': '1000',
        'showEasing': 'swing',
        'hideEasing': 'linear',
        'showMethod': 'fadeIn',
        'hideMethod': 'fadeOut',
    };
    toastr.error(msg, "Error");
}


function showSuccessMessage(msg) {
    toastr.options = {
        'closeButton': true,
        'debug': false,
        'newestOnTop': false,
        'progressBar': false,
        'positionClass': 'toast-top-center',
        'preventDuplicates': false,
        'showDuration': '200',
        'hideDuration': '700',
        'timeOut': '5000',
        'extendedTimeOut': '1000',
        'showEasing': 'swing',
        'hideEasing': 'linear',
        'showMethod': 'fadeIn',
        'hideMethod': 'fadeOut',
    };
    toastr.success(msg, "Success");
}
