﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CreditCars.WebUI.Controllers
{
    [Authorize]
    [Route("reports")]
    public class ReportsController : BaseController
    {
        public async Task<IActionResult> Index()
        {
            return View();
        }
    }
}