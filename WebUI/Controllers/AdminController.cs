﻿using CreditCars.Application.Common.Models;
using CreditCars.Application.CustomerScoreSurvey.Commands.DeleteSurveyAnswer;
using CreditCars.Application.CustomerScoreSurvey.Commands.SaveSurveyQuestion;
using CreditCars.Application.CustomerScoreSurvey.Queries.GetCustomerScoreSurveyQuestions;
using CreditCars.WebUI.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCars.WebUI.Controllers
{
    //REMOVE THIS ONCE ROLES ARE FIXED[Authorize(Roles = "Admin")]
    [Route("admin")]
    public class AdminController : BaseController
    {
        public async Task<IActionResult> Index()
        {
            return RedirectToAction(nameof(ViewQuestions));
        }

        [HttpGet]
        [Route("view-questions")]
        public async Task<IActionResult> ViewQuestions()
        {
            var questionsVM = await Mediator.Send(new GetCustomerScoreSurveyQuestionsQuery());

            var vm = new ViewSurveyQuestionsViewModel()
            {
                Questions = questionsVM.Questions
            };

            return View(vm);
        }

        [HttpGet]
        [Route("edit-question/{id:int?}")]
        public async Task<IActionResult> EditQuestion(int? id)
        {
            var questions = await Mediator.Send(new GetCustomerScoreSurveyQuestionsQuery() { Id = id });
            var questionVM = questions.Questions.FirstOrDefault();

            return View(questionVM);
        }

        [HttpPost]
        [Route("edit-question")]
        public async Task<IActionResult> EditQuestion(SurveyQuestionDto request)
        {
            var questionsVM = await Mediator.Send(new SaveSurveyQuestionCommand() { Question = request });

            // go back to the listing and show the refreshed questions
            return RedirectToAction(nameof(ViewQuestions));
        }

        [HttpPost]
        [Route("delete-answer")]
        public async Task<IActionResult> DeleteAnswer(DeleteSurveyAnswerCommand request)
        {
            var questionsVM = await Mediator.Send(request);

            // go back to the listing and show the refreshed questions
            return Ok(questionsVM);
        }
    }


}
