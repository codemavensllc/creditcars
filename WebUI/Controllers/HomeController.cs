﻿using CreditCars.Application.CustomerScoreSurvey.Queries.GetCustomerScoreSurveyQuestions;
using CreditCars.WebUI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Threading.Tasks;

namespace CreditCars.WebUI.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            //return View();
            return RedirectToAction(nameof(CustomerScoreSurvey));
        }

        [Route("dashboard")]
        public async Task<IActionResult> Dashboard()
        {
            return View();
        }

        [Route("customer-scorecard")]
        [HttpGet]
        public async Task<IActionResult> CustomerScoreSurvey()
        {
            var questionsVM = await Mediator.Send(new GetCustomerScoreSurveyQuestionsQuery());

            var vm = new CustomerScoreSurveyViewModel()
            {
                Questions = questionsVM.Questions
            };

            return View(vm);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        /*
        [Route("congregations-reporting-data")]
        [HttpGet]
        public async Task<JsonResult> GetCongregationReportingData()
        {
            try
            {
                var request = new CongsReportingQuery();
                var result = await Mediator.Send(request);
                return Json(result);
            }
            catch (ValidationException exc)
            {
                var errMessages = new List<string>();
                foreach (var item in exc.Errors)
                {
                    errMessages.Add($"{item.Key}:{String.Join("; ", item.Value)}");
                }

                return Json(Result.Failure(errMessages));
            }
            catch (System.Exception exc)
            {
                return Json(Result.Failure(new List<string>() { exc.Message }));
            }
        }
        */

    }
}
